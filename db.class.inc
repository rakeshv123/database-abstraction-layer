<?php

/************************************************************************************************************************************/
/********@package Database Abstraction Layer
/********@author Rakesh V
/********@description Contains definition for Singleton Database class which is used to connect to the database
/************************************************************************************************************************************/

	include_once("sysconfig.class.inc");	
	
	class Database
	{
		public $link;
		private $conn_str; 
		private static $_instance;
		
		private function __construct() {}
		
		public static function Instance()
		{
			if(!self::$_instance)
			{
				self::$_instance=new Database;
			}
			return self::$_instance;		
		}
		
		public function connect($dbName=false)
		{
			$this->link=@mysql_connect(SysConfig::dbHost,SysConfig::dbUser,SysConfig::dbPassword);
			if(!$this->link)
			{
				throw new Exception('Unable to establish database connection: '.mysql_error());
			}
			
			if ($dbName) $this->useDatabase($dbName);
        
			$version = mysql_get_server_info();
			$this->conn_str = "'$dbName' on ".SysConfig::dbUser."@".SysConfig::dbHost." (MySQL $version)";
		
			return $this->link;
		}
		
		public function getConnectionString()
    		{
        		return $this->conn_str;
    		}
		
		public function useDatabase($dbName)
		{ 
			if (!@mysql_select_db($dbName, $this->link))
 		        {
			        throw new Exception('Unable to select database: ' . mysql_error($this->link));
        		}			
		}		
		
	}

?>
