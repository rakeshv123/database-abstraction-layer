#!/bin/bash
USER=root
PASS=root
DB=jobsxplode
HOST=localhost
TEMP=/tmp/RakeshTemp
TARGET=./gendir

mkdir -p $TARGET
cp default_table.class.inc $TARGET
cp db.class.inc $TARGET

#Generate SysConfig File
FILE=$TARGET/sysconfig.class.inc
echo "<?php" >$FILE;
echo "/**************Generated Class***************/">>$FILE

echo "class SysConfig" >> $FILE
echo "{" >> $FILE
echo -e "\tconst dbHost=\"$HOST\";" >> $FILE
echo -e "\tconst dbUser=\"$USER\";" >> $FILE
echo -e "\tconst dbPassword=\"$PASS\";" >> $FILE
echo "}" >> $FILE
echo "?>" >>$FILE;


echo "SHOW TABLES" > $TEMP
TABLES=`mysql -u$USER -p$PASS -D$DB < $TEMP | tail -n +2`
for table in $TABLES 
do
	echo "DESC $table" > $TEMP
	FieldList=`mysql -u$USER -p$PASS -D$DB < $TEMP | tail -n +2 | cut -f 1 | sed -e "s/^\(.*\)$/\"\1\"/g" | paste -sd ',' | sed -e "s/^\(.*\)$/\(\1\)/g"`
	PrimaryKey=`mysql -u$USER -p$PASS -D$DB < $TEMP | grep "PRI" | cut -f 1`
	FileName=`echo $table | sed -e "s/^/Pd/g" | sed -e "s/[_]\([a-z]\)/\U\1/g"`
	FILE=$TARGET/$FileName.class.inc
	echo "<?php" >$FILE;
	echo "/**************Generated Class***************/">>$FILE

	echo "include_once(\"default_table.class.inc\");" >> $FILE
	echo "class $FileName extends default_table" >> $FILE
	echo "{" >> $FILE
	echo -e "\tfunction __construct()" >> $FILE
	echo -e "\t{" >> $FILE
	
	echo -e "\t\t\$this->tablename=\"$table\";" >>$FILE
	echo -e "\t\t\$this->dbname=\"$DB\";" >>$FILE
	echo -e "\t\t\$this->fieldlist=array$FieldList;" >>$FILE

	if [ ! -z $PrimaryKey ]
	then
		echo -e "\t\t\$this->fieldlist[\"$PrimaryKey\"]=array(\"pkey\"=>\"y\");" >>$FILE
	fi 
	echo -e "\t}" >>$FILE
	echo "}" >> $FILE

	echo "?>" >>$FILE
	
done

rm -f $TEMP
