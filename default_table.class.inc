<?php

/************************************************************************************************************************************/
/********@package Database Abstraction Layer
/********@author Rakesh V
/********@description Contains definition for default_table abstract class from which all DML classes are derived
/************************************************************************************************************************************/
	require_once("db.class.inc");
	class Default_Table
	{
		var $dbname;
		var $tablename;
		var $fieldlist;
		var $data_array;
		var $errors;
		
		function __construct()
		{
			$dbname='jobsxplode';
			$tablename='default';
			$this->fieldlist = array('column1', 'column2', 'column3');
			$this->fieldlist['column1'] = array('pkey' => 'y');
		}	
		
		function getData($where=NULL)
		{
			$this->data_array=array();
			$dbInstance=Database::Instance();
			$dbInstance->connect($this->dbname);
			if (empty($where))
			{
         			$where_str = NULL;
      			}
      			else 
      			{
         			$where_str = "WHERE $where";
                        } 
                        $query="SELECT * FROM $this->tablename $where_str";                                                                        
                        $result=mysql_query($query,$dbInstance->link); 
			if($result)
			{   
		                while($row=mysql_fetch_assoc($result))
		                {                        
		                	$this->data_array[]=$row;
		                }                      
				mysql_free_result($result);
				return $this->data_array;
			}
			else
			{
				return false;
			}
		}
		function genericNonSelectQuery($query)
		{
			$dbInstance=Database::Instance();
			$dbInstance->connect($this->dbname);
			$result=mysql_query($query);
			if($result) return true;
			else return false;
		}
		function genericQuery($query)
		{
			$this->data_array=array();
			$dbInstance=Database::Instance();
			$dbInstance->connect($this->dbname);
			$result=mysql_query($query);
			if($result)
			{
				if(mysql_num_rows($result)==1)
				{
					$this->data_array=mysql_fetch_assoc($result);
				}
				else
				{
					while($row=mysql_fetch_assoc($result))
				        {                        
				        	$this->data_array[]=$row;
				        }                      
				}
			}
			return $this->data_array;
		}
		
		function insertRecord($fieldarray)
		{
	       		$fieldlist = $this->fieldlist;	       		
	       		$dbInstance=Database::Instance();
			$dbInstance->connect('jobsxplode');
      			foreach ($fieldarray as $field => $fieldvalue) 
      			{
			        if (!in_array($field, $fieldlist)) 
			        {
			        	unset ($fieldarray[$field]);
         			} 
      			}       		
      			$query = "INSERT INTO $this->tablename SET ";
      			foreach ($fieldarray as $item => $value) 
      			{
    				$query .= "$item='$value', ";
		        } 
		        $query = rtrim($query, ', ');		
		        $result=mysql_query($query,$dbInstance->link);
		        if(mysql_errno() <> 0)
		        {
		        	if(mysql_errno()==1062)
		        	{
		        		$this->errors="A record already exists with this ID";
		        	}
		        	else
		        	{
		        		//TODO invoke error handler
		        	}
		        }
		        return $result;
		}
		function insertAndGetAutoIncrementKey($fieldarray,$field)
		{
			if($this->insertRecord($fieldarray))
			{
				$dbInstance=Database::Instance();
				$dbInstance->connect('jobsxplode');
				$query= "SELECT $field FROM $this->tablename ORDER BY $field DESC LIMIT 0,1";
				$result=mysql_query($query,$dbInstance->link);
				$row=mysql_fetch_row($result);
				return $row[0];			
			}
			return 0;
		}
		
		function updateRecord($fieldarray)
		{
			$fieldlist = $this->fieldlist;
	       		$dbInstance=Database::Instance();
			$dbInstance->connect('jobsxplode');
			
      			foreach ($fieldarray as $field => $fieldvalue) 
      			{
			        if (!in_array($field, $fieldlist)) 
			        {
			        	unset ($fieldarray[$field]);
         			} 
      			}       
      			
      			$where  = NULL;
			$update = NULL;
			foreach ($fieldarray as $item => $value) 
			{
				if (isset($fieldlist[$item]['pkey'])) 
				{
					$where .= "$item='$value' AND ";
			 	}
			 	else
			 	{
				        $update .= "$item='$value', ";
				 } 
     			 }
     			 $update=rtrim($update,", ");
     			 $where=rtrim($where," AND ");
     			 $query="UPDATE $this->tablename SET $update WHERE $where";
     			 $result=mysql_query($query,$dbInstance->link);
    			 return $result;
			 //return $dbInstance->link;
    		}
    		
    		function deleteRecord($fieldarray)
    		{
    			$fieldlist=$this->fieldlist;
    			
    			$dbInstance=Database::Instance();
			$dbInstance->connect('jobsxplode');
			
			$where  = NULL;
			foreach ($fieldarray as $item => $value) 
			{
				if (isset($fieldlist[$item]['pkey'])) 
				{
					$where .= "$item='$value' AND ";
			 	}
			 }
			 $where=rtrim($where," AND ");
     			 $query="DELETE FROM $this->tablename WHERE $where";
     			 $result=mysql_query($query,$dbInstance->link);
     			 mysql_free_result($result);
    			 return;			
    		}
    		
    		function locateRecord($fieldarray)
    		{
	    		$fieldlist=$this->fieldlist;    			
   			
			$where  = NULL;
			foreach ($fieldarray as $item => $value) 
			{
				if (isset($fieldlist[$item]['pkey'])) 
				{
					$where .= "$item='$value' AND ";
			 	}
			 }
			 $where=rtrim($where," AND ");
			 
			 $rows=$this->getData($where);    			
			 if($rows)
			 {
			 	return $rows[0];
			 }
			 else
			 {
			 	return false;
			 }
    		
    		}
    		
    		
	}

?>
